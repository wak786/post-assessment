package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class TeamMember {

    private String name;
    private Map<String, Set<Slot>> calendar;
    private MeetingAssistant meetingAssistant = new MeetingAssistant();

    public TeamMember(String name, Map<String, Set<Slot>> calendar) {
        this.name = name;
        this.calendar = calendar;
    }

    public List<Slot> getFreeSlots(LocalDateTime localDateTime) {
        Set<Slot> slots = EnumSet.allOf(Slot.class);
        if (calendar.get(localDateTime.toLocalDate().toString()) != null)
            slots.removeAll(calendar.get(localDateTime.toLocalDate().toString()));
        return slots.stream().filter(slot -> slot.getTime() <= localDateTime.toLocalTime().getHour()).collect(Collectors.toList());
    }

    public MeetingAssistant getMeetingAssistant() {
        return meetingAssistant;
    }
}
