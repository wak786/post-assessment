package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class MeetingAssistant {

    public String scheduleMeeting(List<TeamMember> teamMembers, LocalDateTime dateTime) {
        List<Slot> freeSlotsForAMember = teamMembers.get(0).getFreeSlots(dateTime);
        for (Slot slot : freeSlotsForAMember) {
            List<TeamMember> availMembers = teamMembers.stream().filter(teamMember -> teamMember.getFreeSlots(dateTime).contains(slot)).collect(Collectors.toList());
            if (teamMembers.size() == availMembers.size()) {
                return "Slot available " + slot.getTime();
            }
        }
        return "No Slot available";
    }
}
