package com.xnsio.cleancode;

public enum Slot {
    Eight(8),
    Nine(9),
    Ten(10),
    Eleven(11),
    Twelve(12),
    One(13),
    Two(14),
    Three(15),
    Four(16);

    private int time;

    Slot(int time) {
        this.time = time;
    }

    public int getTime() {
        return this.time;
    }
}
