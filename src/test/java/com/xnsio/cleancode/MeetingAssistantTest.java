package com.xnsio.cleancode;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static com.xnsio.cleancode.Slot.*;
import static java.util.EnumSet.allOf;
import static java.util.EnumSet.of;

public class MeetingAssistantTest {

    private TeamMember MINI;
    private TeamMember BRAD;
    private TeamMember JANET;
    private TeamMember FRANK;
    private TeamMember BUSY_BEE;

    @Before
    public void setUp() {
        Map<String, Set<Slot>> minisCalendar = new HashMap();
        minisCalendar.put(LocalDate.now().toString(), of(Eight, Nine, Twelve, Three, Four));
        MINI = new TeamMember("Mini", minisCalendar);

        Map<String, Set<Slot>> frankCalendar = new HashMap();
        frankCalendar.put(LocalDate.now().toString(), of(Eight, Nine, Eleven, Three, Four));
        FRANK = new TeamMember("Frank", frankCalendar);

        Map<String, Set<Slot>> janetsCalendar = new HashMap();
        janetsCalendar.put(LocalDate.now().toString(), of(Eight, Nine));
        JANET = new TeamMember("Janet", janetsCalendar);

        Map<String, Set<Slot>> busyBeesCalendar = new HashMap<>();
        busyBeesCalendar.put(LocalDate.now().toString(), allOf(Slot.class));
        BUSY_BEE = new TeamMember("Busy Bee", busyBeesCalendar);
    }

    @Test
    public void shouldScheduleMeetingForMiniAndFrank() {
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(17, 0));
        Assert.assertEquals("Slot available 10", MINI.getMeetingAssistant().scheduleMeeting(Arrays.asList(FRANK, MINI), dateTime));
    }

    @Test
    public void shouldNotScheduleMeetingWithBusyBee() {
        List<TeamMember> teamMembers = Arrays.asList(JANET, BUSY_BEE);
        LocalDateTime dateTime = LocalDateTime.now();
        Assert.assertEquals("No Slot available", JANET.getMeetingAssistant().scheduleMeeting(teamMembers, dateTime));
        Assert.assertEquals("No Slot available", MINI.getMeetingAssistant().scheduleMeeting(teamMembers, dateTime));
        Assert.assertEquals("No Slot available", FRANK.getMeetingAssistant().scheduleMeeting(teamMembers, dateTime));
    }
}
